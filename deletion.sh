#!/bin/bash

psw=`cat cred.txt`

while read line
    do
        sshpass -p $psw ssh -n -T itunix@$line "sudo su - root -c 'rm /home/itunix/temp_sudo.sh'"
done < servers.txt