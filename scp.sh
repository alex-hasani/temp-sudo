#!/bin/bash
echo "" > verify_log.txt
echo "" > final_log.txt
psw=`cat cred.txt`

#Reading the variables
#reading_info(){
#	echo "Please enter the Work Order or REQ number: "
#	read wo
#
#	echo "Please enter the -a account which you want to add to the server: "
#	read acc
#
#	echo "How long do you need this permission (Please define the number of dayss. up to 14 days) : "
#	read length
#}

#Copying the file
copy(){
        echo -e "===============================================\n\nNow We are copying the file to $line.\n\n==============================================="
        echo y | pscp -pw $psw ./temp_sudo.sh itunix@$line:/home/itunix/ | tee ./verify_log.txt
        echo "Report for $line" >> final_log.txt
        cat ./verify_log.txt >> final_log.txt
        sshpass -p $psw ssh -n -T itunix@$line "chmod ug+wx /home/itunix/temp_sudo.sh"
        echo -e "===============================================\n\nCopying the file has been completed to $line.\n\n==============================================="
}

#verifying the copied file
verify(){
        verif=$( grep "%" ./verify_log.txt | awk '{print $12}' )
        if [[ $verif == "100%" ]]
        then echo -e "===============================================\n\nThe file has been copied to $line successfully.\n\n===============================================\n"
        else echo -e "===============================================\n\nThe file might've not been copied to $line successfully.\n\n===============================================\n"
        fi
#        echo $psw | ssh $line 'ls -lha /home/itunix/temp_sudo.sh'
        echo -e "===============================================\n Now We are verifying the file on $line.\n===============================================\n"
        sshpass -p $psw ssh -n -T itunix@$line "ls -lha /home/itunix/temp_sudo.sh" 
        echo -e "\n===============================================\n Verification completed on $line.\n===============================================\n"
        sleep 1s
}

#Implementing the sudo access
#imp(){
#       echo "========== Please refer to the information below and use them equentially =========="
#       echo "1. Run: sudo su - "
#       echo "2. Copy & Paste: cd /home/itunix/ && ./temp_sudo.sh"
#       echo "3. Copy & Paste: $wo"
#       echo "4. Copy & Paste: $acc"
#       echo "5. Copy & Paste: $length"
#       echo "6. In the end Type & Enter (twice): exit"
#       echo "====================================================================================="
#       #ssh itunix@$line "sudo su - root -c 'bash /home/itunix/temp_sudo.sh'" < $wo $acc $length
#       sleep 2s
#       sshpass -p $psw ssh $line "sudo su - root -c 'chmod ug+wx /home/itunix/temp_sudo.sh;cd /home/itunix/;./temp_sudo.sh;rm -f /home/itunix/temp_sudo.sh'"
#}

#Main

#reading_info
#sleep 2s
while read line
        do
        copy
        sleep 1s
        verify
        sleep 1s
#        imp
#        sleep 2s
done < servers.txt